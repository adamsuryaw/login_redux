//Langkah 10. Meng-import Provider dari react-redux yang digunakan untuk memanggil props store dari file index.js pada folder store.js
//Langkah 11. Meng-import BrowserRouter dari react-router-dom yang digunakan untuk mengakses halaman lain
//Langkah 12. Memanggil halaman Login dengan tag Route

import './App.css';
import logo from './logo.svg'
import { Provider } from 'react-redux';
import store from './redux/store/index';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Login from './pages/Login';
import Register from './pages/Register';
import Home from './pages/Home'
import About from './pages/About'
// import { useSelector } from 'react-redux';

function App() {

  // const globalState = useSelector((state) => state)
  // console.log(globalState)

  return (
    <Provider store={store}>
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit vscode <code>src/App.js</code> and save to reload
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noonpener noreferrer"
            data-testid="text-a"
          >
            A
          </a>
        </header>
        <Router>
          <Switch>
            <Route path='/' exact component={Login} />
            <Route path='/home' component={Home} />
            <Route path='/register' component={Register} />
            <Route path='/about' component={About} />
          </Switch>
        </Router>
      </div>
    </Provider>
    
  );
}

export default App;
