import { useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

const Register = () => {
    const [name, setName] = useState();
    const [username, setUsername] = useState();
    const [password, setPassword] = useState();

    const addData = async () => {
        try {
            const dataInput = {
                name, //ini sama dengan name: name tapi disingkat
                username,
                password
            }
            const response = await axios.post(`${process.env.REACT_APP_BASE_API_URL}/auth/register`, dataInput)
            console.log(response)
        } catch (err) {
            console.error(err);
        }
    }

    // console.log(name)
    // console.log(username)
    // console.log(password)
    return (
        <div>
            <h1>Halaman Register</h1>
            <input 
                type="text" 
                placeholder="Masukkan nama" 
                onChange={(e) => setName(e.target.value)}
            />
            <br />
            <br />
            <input 
                type="text" 
                placeholder="Masukkan username"
                onChange={(e) => setUsername(e.target.value)}
            />
            <br />
            <br />
            <input 
                type="password" 
                placeholder="Masukkan password"
                onChange={(e) => setPassword(e.target.value)}
            />
            <br />
            <br />
            <button onClick={() => addData()}>Submit</button>
            <Link to="/">Go to Login</Link>
        </div>
        
    )
}

export default Register;