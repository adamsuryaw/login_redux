import { Link, useHistory } from 'react-router-dom';
import { useState } from 'react';
import axios from 'axios';
import { useDispatch } from 'react-redux';

const Login = () => {
    const [username, setUsername] = useState();
    const [password, setPassword] = useState();
    const dispatch = useDispatch();
    const history = useHistory();

    const checkLogin = async () => {
        try {
            const dataInput = {
                username,
                password
            }
            const respone = await axios.post(`${process.env.REACT_APP_BASE_API_URL}/auth/login`, dataInput)
            console.log(respone);
            localStorage.setItem('token', respone.data.token);
            // dispatch({ type: "IS_LOGIN", payload: true})
            history.push("/home")
        } catch (err) {
            console.error(err);
        }
    }

    return (
        <>
            <h1>Halaman Login</h1>
            <input 
                type="text" 
                placeholder="Masukkan username"
                onChange={(e) => setUsername(e.target.value)}
            />
            <br />
            <br />
            <input 
                type="password" 
                placeholder="Masukkan password"
                onChange={(e) => setPassword(e.target.value)}
            />
            <br />
            <br />
            <button onClick={() => checkLogin()}>Submit</button>
            <Link to="/register">Go to Register</Link>
        </>
    )
}

export default Login;