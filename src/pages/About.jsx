import { useSelector } from 'react-redux';

const About = () => {
    const globalState = useSelector((state) => state)
    console.log(globalState)
    return (
        <> 
            <h1>Halaman About</h1>
            {globalState.auth.name}
            {globalState.auth.username}
        </>
    )
}

export default About;