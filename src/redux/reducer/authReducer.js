//Langkah 1. Membuat initialState yang berisi object
//Langkah 2. Membuat function dengan parameter initialState dan action
//Langkah 3. Membuat destructive object(memanggil property dalam object) dari object action
//Langkah 4. Membuat switch case dengan parameter type
import { SET_NAME, SET_USERNAME } from "./../typeAction";

const globalState = {
  name: "",
  username: "",
  // isLogin: false,
};

const authReducer = (state = globalState, action) => {
  const { type, payload } = action;
  switch (type) {
    case SET_NAME: {
      return {
        ...state,
        name: payload,
      };
    }
    // case "IS_LOGIN": {
    //     return {
    //         ...state,
    //         isLogin: payload,
    //     }
    // }
    case SET_USERNAME: {
      return {
        ...state,
        username: payload,
      };
    }
    default: {
      return {
        ...state,
      };
    }
  }
};

export default authReducer;
