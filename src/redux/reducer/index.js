//Langkah 5. Meng-import combineReducers untuk menggabungkan reducer
//Langkah 6. Membuat function bernama combineReducers yang berisi object auth
//Langkah 7. Mengisi nilai auth dengan authReducer dari file authReducer.js untuk memanggil datanya

import {combineReducers} from 'redux';
import authReducer from './authReducer';

export default combineReducers({
    auth: authReducer,
})