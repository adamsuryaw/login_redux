import { SET_NAME, SET_USERNAME } from './../typeAction'
import axios from 'axios'
import { useDispatch } from 'react-redux'

const currentToken = localStorage.getItem("token")
const config = {
    headers: {
        Authorization: `Bearer ${currentToken}`
    }
}

export const getName = (payload) => {
    return {
        type: SET_NAME,
        payload: payload
    }
}

export const getUsername = (payload) => {
    return async (dispatch) => {
        const respone = await axios.get(
                `${process.env.REACT_APP_BASE_API_URL}/auth/profile`,
                config);
        dispatch({ 
            type: SET_USERNAME, 
            payload: respone.data.result.username})
    }
    
}