//Langkah 8. Meng-import createStore untuk membuat variabel store yang digunakan untuk memanggil reducer yang sudah digabung pada file index.js pada folder reducer
//Langkah 9. Membuat variabel bernama store yang berisi rootReducer, rootReducer ini adalah gabungan reducer dari file index.js pada folder reducer

import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './../reducer';

const store = createStore(rootReducer, applyMiddleware(thunk));
export default store;